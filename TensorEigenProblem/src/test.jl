using LinearAlgebra

cd(@__DIR__)

# Tests for tenvec.jl
include("./tenvec.jl")
include("./test_tenvec.jl")

m, n = 4, 3
dims = fill(n, m)
A = rand(-3:3, dims...)
x = rand(-3:3, n)

Y1 = tenvec(A, x, 1)
Y1_exact = tenvec_4_1(A, x)
@assert Y1 == Y1_exact

Y2 = tenvec(A, x, 2)
Y2_exact = tenvec_4_2(A, x)
@assert Y2 == Y2_exact

Y3 = tenvec(A, x, 3)
Y3_exact = tenvec_4_3(A, x)
@assert Y3 == Y3_exact

println("Testing passed! ヾ(oﾟωﾟo)ﾉﾞ")