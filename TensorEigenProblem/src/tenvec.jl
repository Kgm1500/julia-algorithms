using LinearAlgebra
using Base.Cartesian
using Base.Meta


"""
    tenvec(A, x, k)

Contracting a tensor A and a vector x (k times).
See, `tenvec!` for more details.
"""
function tenvec(A, x, k)
    m = length(size(A))
    n = size(A, 1)
    dims = fill(n, m-k) # ⇔ [n, n, n, ..., n]
    Y = zeros(typeof(A[1]), dims...) # ⇔ zeros(n, n, n, ..., n)
    tenvec!(Y, A, x)
    return Y
end


"""
    tenvec!(Y, A, x)
"""
@generated function tenvec!(Y::Array{T,l}, A::Array{T,m}, x::Array{T,1}) where {T, l, m}
    m_minus_l = m - l
    quote
        Y .= 0
        @inbounds @nloops $m i A begin
            tmp = @nref $m A i
            # ⇔ tmp = A[i_1,...,i_m]
            
            @nexprs $m_minus_l k->(tmp *= x[i_{k+$l}])
            # ⇔
            # for k = 1:m-l
            #     tmp *= x[i_{k+l}]
            # end
            
            (@nref $l Y i) += tmp
            # ⇔ Y[i_1,...,i_l] = tmp
        end
    end
end


