function tenvec_4_1(A, x)
    n = size(A,1)
    Y = zeros(n, n, n)
    for i = 1:n
        for j = 1:n
            for k = 1:n
                for l = 1:n
                    Y[i,j,k] += A[i,j,k,l] * x[l]
                end
            end
        end
    end
    return Y
end


function tenvec_4_2(A, x)
    n = size(A,1)
    Y = zeros(n, n)
    for i = 1:n
        for j = 1:n
            for k = 1:n
                for l = 1:n
                    Y[i,j] += A[i,j,k,l] * x[k] * x[l]
                end
            end
        end
    end
    return Y
end


function tenvec_4_3(A, x)
    n = size(A,1)
    Y = zeros(n)
    for i = 1:n
        for j = 1:n
            for k = 1:n
                for l = 1:n
                    Y[i] += A[i,j,k,l] * x[j] * x[k] * x[l]
                end
            end
        end
    end
    return Y
end